﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_Asp_ReadOnly
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtfirstname.Text = "Mangal Singh";
            txtlastname.Text = "Gill";
            txtage.Text = "21";

        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            txtfirstname.ReadOnly = false;
            txtlastname.ReadOnly = false;
            txtage.ReadOnly = false;
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtfirstname!=null && txtlastname!=null)
            {
                Response.Redirect("~/Default2.aspx");
               
            }
            else
            {
                Response.Redirect("~/Default.aspx");

            }
        }
    }
}