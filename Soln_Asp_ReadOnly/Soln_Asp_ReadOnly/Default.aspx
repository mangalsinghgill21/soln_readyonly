﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Soln_Asp_ReadOnly.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/StyleSheet.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            
            <ContentTemplate>
                
                    
                  <asp:TextBox ID="txtfirstname" runat="server" placeHolder="FirstName" ReadOnly="true" TabIndex="0" CssClass="txt" />

                       <br />
                
                   
                  <asp:TextBox ID="txtlastname" runat="server" placeHolder="LastName" ReadOnly="true" TabIndex="1" CssClass="txt" />
                       <br />
                
                 
                <asp:TextBox ID="txtage" runat="server" placeHolder="Age" ReadOnly="true" TabIndex="2" CssClass="txt" />
                     
                    <br />
                 <asp:Button ID ="btnedit" runat="server" Text="Edit" TabIndex="3" CssClass="btn btn2" OnClick="btnedit_Click" />
                                      
                 <asp:Button ID ="btnsubmit" runat="server" Text="Submit" TabIndex="4" CssClass="btn btn2" OnClick="btnsubmit_Click" />
                        
               
                
            </ContentTemplate>
        </asp:UpdatePanel>
       
        </div>
    </form>
</body>
</html>
